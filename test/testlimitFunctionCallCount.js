let limitFunctionCallCount = require("../limitFunctionalCallCount");


function cb(count) {
  if(count==null){

    console.log("Cannot execute this function anymore")

  }
  else{
  console.log(`Hello ${count}`);
  }
}

const callCbNtimes = limitFunctionCallCount(cb, 2);

callCbNtimes();
callCbNtimes();
callCbNtimes();
callCbNtimes();