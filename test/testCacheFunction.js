let cacheFunction = require("../cacheFunction");

const invokeCB = cacheFunction(cb);

function cb(name) {
  console.log(name);
}

invokeCB("Alen");
invokeCB("Aby");
invokeCB("Alen");

invokeCB("");
invokeCB();
invokeCB([]);
invokeCB(5);
invokeCB("Rocky");
