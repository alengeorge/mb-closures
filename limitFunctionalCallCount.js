function limitFunctionCallCount(cb, n) {

  let limit = 0
  function callCbNtimes() {
    if (n <= limit) {
      cb(null);
    }
    else{

      cb(limit+1)
    }
      ++limit
      
    }

  

  return callCbNtimes;
}

module.exports = limitFunctionCallCount;
