function cacheFunction(cb) {
  let cache = {};

  function invokeCB(param) {
    if (cache.hasOwnProperty(param)) {
      return cb(`${param}, we have already met..see you later`);
    } else if (typeof param === "string" && param.trim().length != 0) {
        cache[param] = param;
      return cb(`Hi ${param}, Nice to meet you`);
      
     
    } else {
        return "Invalid Input---Only accepts non-empty string";
    }
  }

  return invokeCB;
}

module.exports = cacheFunction;
